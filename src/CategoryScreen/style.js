import React, { Component } from 'react';
import { StyleSheet,
         View, 
         Button, 
         Alert, 
         TouchableOpacity, 
         Text, 
         Image } 
         from 'react-native';
import { FlatGrid } from 'react-native-super-grid';

export default class CategoryScreen extends Component {
   static navigationOptions = {
    headerLeft: (
      <Image
        source={require('../../assets/img/logo_icon.png')}
        style={{ width: 125, height: 45, marginLeft: 10 }}
      />
    ),
    title: 'Danh Mục',
    headerStyle: {
      backgroundColor: '#3ee2bf',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: '400',
      marginLeft: 95,
      marginTop: 10,
      fontSize: 28,
      fontFamily: "Lobster-Regular",
    },
    headerRight: (
      <Image
      source={require('../../assets/img/icon_other.png')} 
      style={{ width: 30, height: 30, marginRight: 5, marginTop: 10, opacity: 10}}
    />
    ),
  };
  render() {
    const list = 
    [
      { name: 'Trái Cây', quantity: '20', img: require('../../assets/img/fruit.jpg')},
      { name: 'Gạo', quantity: '10', img: require('../../assets/img/rice.jpg')},
      { name: 'Củ, Quả, Hạt', quantity: '15', img: require('../../assets/img/vegetables.jpg')},
      { name: 'Rau Gia Vị', quantity: '40', img: require('../../assets/img/raugv.jpg')},
      { name: 'Rau Ăn Lá, Thân', quantity: '60', img: require('../../assets/img/vegetables_2.jpg')},
      { name: 'Đồ Nhà Làm', quantity: '11', img: require('../../assets/img/oil.jpg')},
    ];
   
    return (
        <View>
            <Image source={require('../../assets/img/background_wcyan_home.png')} style={styles.backgroundImage} />
            <View style={styles.loginContainer}>
            <FlatGrid 
                itemDimension={240}
                items={list}
                style={styles.gridView}
                onClick={() => {
                  Alert.alert('You tapped the button!');
                }}
                renderItem={({ item, index }) => (  
            <View  style={[styles.itemContainer, { backgroundColor: 'white' }]}>
                <View style={{backgroundColor: '#c7ecee', flex: 1, borderWidth: 2,borderColor: '#e84118', borderRadius: 10, flexDirection: 'row'}}>
                <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center'}}  onPress={() => this.props.navigation.navigate('ListProductfromCategory')}>
                <View>
                    <Image  source={item.img} style={{width: 185, height: 105, borderRadius: 10, marginLeft: 8}} ></Image>
                    </View>
                    </TouchableOpacity>
                  <View style={{flex: 1,justifyContent: 'center', marginTop: 10, marginBottom: 10, marginRight: 10, alignItems: 'flex-start', marginLeft: 8, borderWidth: 2, borderColor: '#44bd32', borderRadius: 10,}}>
                      <Text style={styles.itemName}>{item.name}</Text>
                      <Text style={styles.itemCode}>Số Lượng: {item.quantity}</Text>
                  </View> 
                   
                </View>
                  
            </View>
        )}
      />
            </View>  
        </View>
    ); 
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
      height: '100%',
      width: '100%'
    },
  loginContainer: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      alignContent: 'center',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
  gridView: {
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    borderWidth: 5,
    borderColor: '#0097e6',
    borderRadius: 10,
    padding: 10,
    height: 150,
  },
  itemName: {
    marginLeft: 10,
    fontSize: 20,
    color: 'blue',
    fontFamily: "Lobster-Regular",
    
  },
  itemCode: {
    marginLeft: 10,
    fontSize: 20,
    color: 'blue',
    fontFamily: "Lobster-Regular",
  },
});