import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TextInput, TouchableHighlight, Alert, ScrollView } from 'react-native';
import { getUserInfoFromServer } from './NetworkAPI/index';


export default class profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "lovelovemm@gmail.com",
            password: "mm",
            userInfoFromServer: [],
            imageUser: ''
        };

    }


    componentDidMount() {

        this.refreshDatafromServer();
    }
    refreshDatafromServer = () => {
        const checkUser = {
            email: this.state.email,
            password: this.state.password
        }
        getUserInfoFromServer(checkUser).then((data) => {
            this.setState({ userInfoFromServer: data })

        }).catch((error) => {
            this.setState({ userInfoFromServer: [] })
        })
        // getQuantityofProductinCategory().then((data)=>{
        //   this.setState({categoryFromServer: data})
        // }).catch((error)=>{
        //   this.setState({categoryFromServer: []})
        // })
    }

    render() {

        return (
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <View style={styles.contentwrap}>
                    <View style={styles.proInfoWrap}>
                        <View style={styles.proInfo}>
                            <Image style={{ width: 394, height: 165, borderRadius: 10, marginLeft: -5, marginTop: -4.5 }} source={{ uri: 'https://images.unsplash.com/photo-1539923779676-1a9bddf986db?ixlib=rb-1.2.1&w=1000&q=80' }}></Image>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                <Image style={{ width: 100, height: 100, borderRadius: 100, marginTop: 8 }} source={{ uri: this.state.imageUser }}></Image>
                            </View>
                            <View style={styles.userInfo}>
                                {this.lapsList()}
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.btnwrap}>
                    <TouchableHighlight
                        style={styles.addcartbtn}
                        onPress={() => {
                            Alert.alert('You tapped the button!');
                        }}
                    >
                        <Text style={styles.addcarttxt}>Edit Profile</Text>
                    </TouchableHighlight>
                </View>
            </View>

        );
    }

    lapsList() {
        return this.state.userInfoFromServer.map((data) => {
           
            return (
                <ScrollView>
                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'https://www.trybooking.com/media/imgs/653488ddd926ab077de00f6d07ada3535e6dc186.png' }}></Image>
                        <Text style={styles.txt}>{data.email}</Text>
                    </View>

                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'https://listimg.pinclipart.com/picdir/s/66-667568_annual-report-gender-icon-transparent-clipart.png' }}></Image>
                        <Text style={styles.txt}>{data.gender}</Text>
                    </View>

                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'http://www.syrahresources.com.au/images/investors/investor-calendar-hover1.png' }}></Image>
                        <Text style={styles.txt}>{data.dateOfBirth}</Text>
                    </View>

                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'https://library.kissclipart.com/20181214/alw/kissclipart-png-phone-icon-turquoise-clipart-mobile-phones-com-c8016facb4230265.jpg' }}></Image>
                        <Text style={styles.txt}>{data.phoneNumber}</Text>
                    </View>

                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'http://www.stickpng.com/assets/images/584856ade0bb315b0f7675ab.png' }}></Image>
                        <Text style={styles.txt}>{data.email}</Text>
                    </View>

                    <View style={styles.item}>
                        <Image style={styles.icon} source={{ uri: 'https://images.vexels.com/media/users/3/140527/isolated/preview/449b95d58f554656b159dd3ca21ab123-home-round-icon-by-vexels.png' }}></Image>
                        <Text style={styles.txt}>{data.address}</Text>
                    </View>
                </ScrollView>
            )
        })

    }
}

const styles = StyleSheet.create({
    contentwrap: {
        position: 'absolute',
        padding: 20,
    },

    proInfoWrap: {
        width: 400,
        height: 630,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        padding: 5,
        marginBottom: 20,
    },

    btn: {
        width: 30,
        height: 30,
        backgroundColor: "#000",
        borderRadius: 100,
        marginTop: 5
    },

    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 600,
    },

    addcartbtn: {
        position: "absolute",
        width: 150,
        height: 40,
        backgroundColor: "#FF6600",
        borderRadius: 100,
        marginTop: 5,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    addcarttxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 18,
        color: "#fff",
        textAlign: "center"
    },

    userInfo: {
        marginTop: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },

    item: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        width: 320,
        height: 45,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#000',
        marginTop: 10
    },

    icon: {
        width: 25,
        height: 25,
        marginTop: 10,
        marginLeft: 20
    },

    txt: {
        marginTop: 10,
        marginLeft: 30,
    }

})